''' abyss

MMO/RPG/RTS/RESTAPI game in your terminal!
'''

from abyss.client import main

if __name__ == '__main__':
    main()
